# Standard library imports
import logging
import shutil
import time
import os

# Local application imports
from unittest import TestCase
from src.mapping.quality_control import QualityControl
from src.settings import pd_models_config


class TestQualityControl(TestCase):

    fasq_dir = "Zackular_EDRN_fastq_files"

    def setUp(self) -> None:
        self.quality_control = QualityControl()

    def test_unzip(self):
        dir_in = os.path.join(pd_models_config["ROOT_DIR"], pd_models_config["inputBasePath"], self.fasq_dir)
        dir_out = dir_in + "1"
        f_type = '*.gz'
        try:
            os.mkdir(dir_out)
        except FileExistsError:
            pass
        self.quality_control.unzip(dir_in, dir_out, f_type)

    def test_RepairReads(self):

        dir_in = os.path.join(pd_models_config["ROOT_DIR"], pd_models_config["inputBasePath"], self.fasq_dir + "1")
        dir_out = os.path.join(pd_models_config["ROOT_DIR"], pd_models_config["inputBasePath"], "fixed_" + self.fasq_dir)
        self.quality_control.repair_reads(dir_in, dir_out)
