# Standard library imports
import logging
import shutil
import time
import os
import numpy as np

# Local application imports
import unittest

from matplotlib import pyplot

from src.parkinson_disease_detection import ParkinsonDiseaseDetection
from src.utils.decorators import function_timing, tag, kill_obj, log_method
from src.settings import pd_models_config


def get_resource_path(filename):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", filename)


class TestParkinsonDiseaseDetection(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        filename = os.path.join("inputs", "parkinsons.data")
        data_file = get_resource_path(filename)
        cls.pd_detection = ParkinsonDiseaseDetection(data_file=data_file)

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def setUp(self) -> None:
        method = getattr(self, self._testMethodName)
        tags = getattr(method, 'tags', {})
        if 'skip_setup' in tags:
            pass

    def test_GetFeaturesLabels_importDataFile_GetExpectedFeatures(self):
        self.pd_detection.split_df_into_x_and_y(column_name='status')
        self.pd_detection.get_features_labels()

        self.assertEqual(self.pd_detection.labels[self.pd_detection.labels == 1].shape[0], 147)
        self.assertEqual(self.pd_detection.labels[self.pd_detection.labels == 0].shape[0], 48)

    def test_splitTrainingTestingSets(self):
        self.pd_detection.split_df_into_x_and_y(column_name='status')
        self.pd_detection.get_features_labels()
        self.pd_detection.initialize_min_max_scaler()
        self.pd_detection.split_training_testing_sets(test_size=0.2,
                                                      random_state=7)

        self.assertEqual(len(self.pd_detection.x_train), 156)
        self.assertEqual(len(self.pd_detection.y_train), 156)
        self.assertEqual(len(self.pd_detection.x_test), 39)
        self.assertEqual(len(self.pd_detection.y_test), 39)

    @unittest.skip('Not ready yet')
    def test_fitGradientBoostingModel(self):
        self.pd_detection.split_df_into_x_and_y(column_name='status')
        self.pd_detection.get_features_labels()
        self.pd_detection.initialize_min_max_scaler()
        self.pd_detection.split_training_testing_sets(test_size=0.2,
                                                      random_state=7)
        self.pd_detection.initialize_xgb_classifier()
        self.pd_detection.fit_gradient_boosting_model()

        # TODO test self.pd_detection.model

    def test_setupWorkflow(self):
        accuracy_score = self.pd_detection.setup_workflow(test_size=0.2,
                                                          random_state=7)

        self.assertAlmostEqual(accuracy_score, 0.923076923)

    def load_additional_dataset(self):
        filename = os.path.join("inputs", "breast-cancer.csv")
        data_file = get_resource_path(filename)
        self.pd_detection = ParkinsonDiseaseDetection(data_file=data_file)
        self.pd_detection.remove_columns('node-caps')
        self.pd_detection.remove_columns('breast-quad')

        self.pd_detection.split_df_into_x_and_y(column_name='class')
        self.pd_detection.get_features_labels()

    def test_GetFeaturesLabels_importDataFile_GetExpectedClassInstances(self):
        """

        :return:
        """

        self.load_additional_dataset()
        self.pd_detection.split_df_into_x_and_y(column_name='class')
        self.pd_detection.get_features_labels()
        self.assertEqual(self.pd_detection.labels[self.pd_detection.labels == 'no-recurrence-events'].shape[0], 201)
        self.assertEqual(self.pd_detection.labels[self.pd_detection.labels == 'recurrence-events'].shape[0], 85)

    def test_EncodeCategoricalClassValuesAsIntegers_ColumnNamebEqualBreast_LabelEncoderClasses(self):
        """

        :return:
        """
        self.load_additional_dataset()
        feature, label_encoder = self.pd_detection.encode_categorical_class_values_as_integers(column_name='breast')
        self.assertListEqual(list(label_encoder.classes_), ['left', 'right'])

    @tag('skip_setupclass')
    def test_EncodeCategoricalClassValuesAsIntegers_ColumnNameEqualBreast_ReturnNumpyArrayOf286Elements(self):
        self.load_additional_dataset()
        feature, label_encoder = self.pd_detection.encode_categorical_class_values_as_integers(column_name='breast')
        self.assertEqual(len(feature), 286)

    def test_EncodeLabelClasses_ColumnNameBreastAndLeftRight_ReturnConcatenatedList(self):
        encoded_class_labels = self.pd_detection.encode_label_classes(column_name='breast',
                                                                      classes=np.array(['left', 'right']))
        expected_list = ['breast_left', 'breast_right']
        self.assertListEqual(encoded_class_labels, expected_list)

    def test_EncodeStringInputValuesAsIntegers_QuantitativeFeatures_ConcatenatedClassesAndFeatures(self):
        self.load_additional_dataset()
        encoded_x, label_encoder_classes = self.pd_detection.encode_string_input_values_as_integers()
        expected_part_list = ['age_20-29', 'age_30-39', 'age_40-49', 'age_50-59', 'age_60-69', 'age_70-79']
        self.assertTrue(set(expected_part_list).issubset(label_encoder_classes))

    def test_EncodeStringInputValuesAsIntegers_QuantitativeFeatures_EncodedXOnlyBinary(self):
        self.load_additional_dataset()
        encoded_x, label_encoder_classes = self.pd_detection.encode_string_input_values_as_integers()
        self.assertTrue(np.array_equal(encoded_x, encoded_x.astype(bool)))

    def test_binary_classification(self):
        self.load_additional_dataset()
        accuracy_score = self.pd_detection.binary_classification()
        print(accuracy_score)
        feature_names = self.pd_detection.label_encoder_classes
        print(self.pd_detection.plot_feature_importances(feature_names))
