import pandas as pd
import os, sys
from matplotlib import pyplot
import numpy as np

try:
    from sklearn.preprocessing import MinMaxScaler, LabelEncoder, OneHotEncoder
    from xgboost import XGBClassifier, plot_importance, plot_tree
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import accuracy_score
    from sklearn.feature_selection import SelectFromModel
except OSError:
    pass


class ParkinsonDiseaseDetection:

    def __init__(self, data_file, sep=','):
        self.data_file = data_file
        self.df = pd.read_csv(self.data_file, sep=sep).replace('"', '', regex=True)
        self.features = None
        self.labels = None
        self.x = None
        self.y = None
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.model = None
        self.scaler = None
        self.y_pred = None
        self.label_encoder_classes = None

    def split_df_into_x_and_y(self, column_name):

        self.x = self.df.loc[:, self.df.columns != column_name]
        self.y = self.df.loc[:, column_name]

    def get_features_labels(self):
        self.features = self.x.values[:, 1:]
        self.labels = self.y.values

    def remove_columns(self, column_name):
        self.df.drop(column_name, axis=1, inplace=True)

    def initialize_min_max_scaler(self):
        self.scaler = MinMaxScaler((-1, 1))

    def split_training_testing_sets(self, test_size, random_state):
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            self.scaler.fit_transform(self.features),
            self.labels, test_size=test_size,
            random_state=random_state)

    def initialize_xgb_classifier(self):
        self.model = XGBClassifier()

    def fit_gradient_boosting_model(self):
        self.model.fit(self.x_train, self.y_train)

    def generate_y_pred(self):
        self.y_pred = self.model.predict(self.x_test)

    def get_feature_importances(self):
        return self.model.feature_importances_

    def setup_workflow(self, test_size, random_state):
        self.remove_columns('name')
        self.split_df_into_x_and_y(column_name='status')
        self.get_features_labels()
        self.initialize_min_max_scaler()
        self.split_training_testing_sets(test_size=test_size, random_state=random_state)
        self.initialize_xgb_classifier()
        self.fit_gradient_boosting_model()
        self.generate_y_pred()
        return accuracy_score(self.y_test, self.y_pred)

    def plot_feature_importances(self, feature_names):
        # set the feature names on XGBoost Booster
        self.model.get_booster().feature_names = feature_names

        # plot features ordered by their importance
        plot_importance(self.model)
        pyplot.show()

    def train_model_using_threshold(self, thresh):
        # select features using threshold
        selection = SelectFromModel(self.model, threshold=thresh, prefit=True)
        select_x_train = selection.transform(self.x_train)
        # train model
        selection_model = XGBClassifier()
        selection_model.fit(select_x_train, self.y_train)
        # eval model
        select_x_test = selection.transform(self.x_test)
        y_pred = selection_model.predict(select_x_test)
        predictions = [round(value) for value in y_pred]
        accuracy = accuracy_score(self.y_test, predictions)
        return accuracy

    def fit_model_using_importance_as_threshold(self):
        for thresh in np.sort(self.model.feature_importances_):
            accuracy = self.train_model_using_threshold(thresh)
            print(accuracy)

    def plot_decision_tree(self):
        plot_tree(self.model, num_trees=0, rankdir='LR')
        pyplot.show()

    def fit_label_encoder_and_return_encoded_labels(self, array):

        label_encoder = LabelEncoder()
        label_encoded_aray = label_encoder.fit_transform(array)
        return label_encoded_aray, label_encoder

    def encode_categorical_class_values_as_integers(self, column_name):
        # encode string class values as integers

        array = self.df.loc[:, column_name].values
        feature, label_encoder = self.fit_label_encoder_and_return_encoded_labels(array)
        feature = feature.reshape(self.df.shape[0], 1)
        return feature, label_encoder

    @staticmethod
    def encode_label_classes(column_name, classes):

        encoded_class_labels = map(lambda x: "{0}_{1}".format(column_name, str(x)), classes)
        return list(encoded_class_labels)

    def encode_string_input_values_as_integers(self):
        """

        :return:
        """

        encoded_x = None
        label_encoder_classes = None
        for column_name in self.x.columns:
            feature, label_encoder = self.encode_categorical_class_values_as_integers(column_name=column_name)

            encoded_class_labels = self.encode_label_classes(column_name, label_encoder.classes_)

            onehot_encoder = OneHotEncoder(sparse=False, categories='auto')
            feature = onehot_encoder.fit_transform(feature)
            if encoded_x is None:
                encoded_x = feature
                label_encoder_classes = encoded_class_labels
            else:
                encoded_x = np.concatenate((encoded_x, feature), axis=1)
                label_encoder_classes.extend(encoded_class_labels)
        return encoded_x, label_encoder_classes

    def binary_classification(self):

        self.split_df_into_x_and_y(column_name='class')

        encoded_x, self.label_encoder_classes = self.encode_string_input_values_as_integers()
        label_encoded_y, y_label_encoder = self.fit_label_encoder_and_return_encoded_labels(array=self.labels)

        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            encoded_x,
            label_encoded_y, test_size=0.2,
            random_state=7)

        self.initialize_xgb_classifier()
        self.fit_gradient_boosting_model()
        self.generate_y_pred()
        return accuracy_score(self.y_test, self.y_pred)

