import glob
import gzip
import os
import shutil
import re
from src.settings import pd_models_config


def get_resource_path(filename):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", filename)


class QualityControl:
    repair_path = '../SW/bbmap/current/ jgi.SplitPairsAndSingles'
    reg_expression = "*R1_001.fastq"
    fasq_dir = "Zackular_EDRN_fastq_files"

    trim_path = '../../SW/Trimmomatic-0.36/trimmomatic-0.36.jar'
    ref_file = '../../SW/Trimmomatic-0.36/adapters/TruSeq3-PE.fa'
    merge_path = '../../../SW/bbmap/current/ jgi.BBMerge'

    def __init__(self):

        pass

    def unzip(self, dir_in, dir_out, f_type):

        for file in glob.glob(os.path.join(dir_in, f_type)):
            if not os.path.isdir(file):
                base = os.path.basename(file)
                f_name = os.path.splitext(base)[0]

                file_out = os.path.join(dir_out, f_name)
                with gzip.open(file, 'r') as f_in, open(file_out, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)

    def fix_reads(self, f_in,
                  r_in,
                  pf_out,
                  pr_out):
        """
        fix files of paired reads that became disordered
        :param f_in:
        :param r_in:
        :param pf_out:
        :param pr_out:
        :return:
        """
        command_line = 'java -ea -Xmx1g -cp {0} in1={1} in2={2} out1={3} out2={4}'.format(self.repair_path,
                                                                                          f_in,
                                                                                          r_in,
                                                                                          pf_out,
                                                                                          pr_out)
        os.system(command_line)

    def trim_reads(self, f_in, f_out):

        command_line = 'java -jar {0} SE -phred33 {1} {2} ILLUMINACLIP:{3}:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36'.format(
            self.trim_path, f_in, f_out, self.ref_file)
        os.system(command_line)

    def merge_reads(self, fwd_file, rev_file, m_file, um_file, ihist):
        ''' Use jgi.BBMerge to merge two overlapping paired reads into a single read
        m_file = merged file
        um_file = unmerged file
        '''

        command_line = 'java -ea -Xmx1g -cp {0} in1={1} in2={2} out={3} outu={4} ihist={5}'.format(self.merge_path,
                                                                                                   fwd_file,
                                                                                                   rev_file,
                                                                                                   m_file,
                                                                                                   um_file,
                                                                                                   ihist)
        os.system(command_line)

    def filter_reads(self):

        """

        :return:
        """

    def repair_reads(self, dir_in, dir_out):

        try:
            os.mkdir(dir_out)
        except FileExistsError:
            pass

        for file in glob.glob(os.path.join(dir_in, self.reg_expression)):
            if not os.path.isdir(file):
                sample_name = re.sub(self.reg_expression[1:], "", os.path.basename(file))
                f_name = os.path.basename(file)
                r_name = '{0}R2_001.fastq'.format(sample_name)

                f_in = os.path.join(dir_in, f_name)
                r_in = os.path.join(dir_in, r_name)

                pf_out = os.path.join(dir_out, f_name)
                pr_out = os.path.join(dir_out, r_name)
