# Standard library imports
import functools
import logging
from time import time
import gc
import asyncio
import warnings

logger = logging.getLogger(__name__)


def function_timing(f):
    def wrapper(*args):
        time1 = time()
        ret = f(*args)
        time2 = time()
        logger.debug(
            "{:s} function took {:.3f} ms".format(f.__name__, (time2 - time1) * 1000.0)
        )
        return ret

    return wrapper


def log_method(method):
    """
    log the call of the method without args and kwargs
    :param method:
    :return:
    """

    def _wrapper(self, *args, **kwargs):
        logger.debug(
            "calling {0}.{1} function".format(self.__module__, method.__name__)
        )
        returnval = method(self, *args, **kwargs)
        logger.debug("called {0}.{1} function".format(self.__module__, method.__name__))
        return returnval

    return _wrapper


def log_function(func):
    """
    log the call of the function without args and kwargs
    :param func:
    :return:
    """

    def _wrapper(*args, **kwargs):
        logger.debug("calling {0}.{1} function".format(func.__module__, func.__name__))
        returnval = func(*args, **kwargs)
        logger.debug("called {0}.{1} function".format(func.__module__, func.__name__))
        return returnval

    return _wrapper


def decorator_passing_arguments(f):
    """
    inform function name and passed args
    :param f:
    :return:
    """

    def _wrapper(*args):
        print("{0}: {1}".format(f.__name__, str(*args)))
        ret = f(*args)
        return ret

    return _wrapper


def synchronized(lock):
    """ Synchronization decorator """

    def _wrapper(f):
        @functools.wraps(f)
        def newFunction(*args, **kw):
            with lock:
                return f(*args, **kw)

        return newFunction

    return _wrapper


def async_test(f):
    def wrapper(*args, **kwargs):
        coro = asyncio.coroutine(f)
        future = coro(*args, **kwargs)
        loop = asyncio.new_event_loop()
        loop.run_until_complete(future)

    return wrapper


def trackcalls(func):
    """

    :param func:
    :return:
    """

    @functools.wraps(func)
    def _wrapper(*args, **kwargs):
        _wrapper.has_been_called = True
        return func(*args, **kwargs)

    _wrapper.has_been_called = False
    return _wrapper


def tag(*tags):
    """
    Decorator to add tags to a test class or method.
    """

    def _decorator(obj):
        setattr(obj, "tags", set(tags))
        return obj

    return _decorator


def kill_obj(*cls):
    """
    Del all instances of a class by its type
    :param cls:
    :return:
    """

    def _decorator(current_obj):
        for obj in gc.get_objects():
            if isinstance(obj, cls):
                del obj
        return current_obj

    return _decorator


def force_sync(fn):
    """
    turn an async function to sync function
    """

    @functools.wraps(fn)
    def _wrapper(*args, **kwargs):
        res = fn(*args, **kwargs)
        if asyncio.iscoroutine(res):
            return asyncio.get_event_loop().run_until_complete(res)
        return res

    return _wrapper


def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used."""

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.warn_explicit(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning,
            filename=func.func_code.co_filename,
            lineno=func.func_code.co_firstlineno + 1
        )
        return func(*args, **kwargs)

    return new_func


def disabled(func):
    """
    disables the provided function
    :param func:
    :return:
    """

    def empty_func(*args, **kargs):
        pass

    return empty_func


def info(fname, expected, actual, flag):
    """
    Convenience function returns nicely formatted error/warning msg.
    :param fname:
    :param expected:
    :param actual:
    :param flag:
    :return:
    """
    format = lambda types: ', '.join([str(t).split("'")[1] for t in types])
    expected, actual = format(expected), format(actual)
    msg = "'{}' method ".format(fname) \
          + ("accepts", "returns")[flag] + " ({}), but ".format(expected) \
          + ("was given", "result is")[flag] + " ({})".format(actual)
    return msg
